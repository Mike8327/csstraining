## PreProcessors: 
A preprocessor is a program that takes one type of data and converts it to another type of data (TypeScript is actually an example of what could be called a Javascript preprocessor, in that it takes one type of data (TypeScript code) and compiles it down to another (in this case Javascript)).   
In the case of css preprocessors, they compile down to css. There are four well-known css preprocessors:  

1. [SASS/SCSS](http://sass-lang.com/guide)
2. [LESS](http://lesscss.org/#overview)
3. [Stylus](http://stylus-lang.com/)
4. [PostCSS](https://postcss.org/)

The differences between the four are fairly minor (mostly to do with syntax differences), but it would be a good idea to get an overview of each. 
For now, we'll focus on SCSS (SCSS is the syntax we use, but it is a slightly newer syntax extension of SASS, and is governed by their rules). 

There are a few things that SCSS gives you access to over and above regular CSS expressions: 
- Variables
- Mixins
- Functions
- Conditionals
- Operators

- - - -

### Variables: 
SCSS Variables are ways to store CSS rules for re-use throughout your application. Variables are defined using the `$` symbol.    
A good example (and one very often used in the real-world) is a variables file for your app's colors:  
```
_variables.scss: 
// Scss color Variables

$primary: #4286f4; // Blue
$secondary: #d8d8d8; // Grey
$accent: #f75b1d; // Orange   
```

Now, whenever you want to use one of those colors, it's as easy as: 

```
@import '../assets/atyles/partials/variables'; // Note, you must import the variables partial (more on partials in a bit) in order to use them in that file.

div {
	color: $primary;
	background-color: $secondary;
	border: 1px solid $accent;
}
```
([CodePen](https://codepen.io/Mike8327/pen/ajRoVx).)

A quick note about css variables:    
Variables exist in regular css as well. They're a different beast to SCSS variables, and you can have both separately. They are defined a little different however:  

```
// CSS Variables are typically defined on the root element, oftentimes this is an alias for the html element. However, you can define them on any element you wish

:root {
	--primary: #4286f4; // Blue
	--secondary: #d8d8d8; // Grey
	--accent: #f75b1d; // Orange
}

div {
	color: var(--primary);
	background-color: var(--secondary);
	border: 1px solid var(--accent);
}
```
([CodePen](https://codepen.io/Mike8327/pen/vaVBrG).)

Note that variables (SCSS variables and CSS variables) can be *any* css rule. 

- - - -

### Nesting: 
In CSS, one of the ways to increase specificity of your code is to 'nest' it. For example: 

HTMl:
```
<section class="container">
	<div class="box">
		<ul class="menu">
			<li>Item 1</li>
			<li>Item 2</li>
			<li>Item 3</li>
			<li>Item 4</li>
		</ul>
	</div>
</section>
```

CSS:
```
.box {
	color: green
}

Versus

.container .box .menu li {
	color: green
}
```    
([CodePen](https://codepen.io/Mike8327/pen/zLmYGO).)

In the second example, the selectors have been nested to increase the specificity of the rule. In SCSS, we can nest like this: 

```
.container {
	display: flex;
	justfify-content: center;
	.box {
    width: 100%;
		border: 1px solid black;
		background-color: lightblue;
		.menu {
      		display: flex;
      		justify-content: space-around;
			list-style: none;
			li {
				color: green;
			}
		}
	}
}
```
([CodePen](https://codepen.io/Mike8327/pen/KBGKdr).)    

Nesting in SCSS allows for much neater code, making it far more readable for other developers coming after you.    
However, best-practice is SCSS says to never nest deeper than two levels *unless you specifically need to nest deeper for specificity*.    
This is because the above code will compile to the following: 

```
.container {
	display: flex;
	justfify-content: center;
}
.container .box {
	border: 1px solid black;
	background-color: lightblue;
}
.container .box .menu {
	list-style: none;
}
.container .box .menu li {
	color: green;
}
```

As you can see, each nested selector compiles to nested CSS. This means that it can be very easy to accidentally create major specificity problems in your CSS. 

- - - -

### Partials and imports  
In CSS, you can use a directive called `@import` to import another CSS file into your own. This can be useful to help with modulating your CSS for better readability   
and easier maintenance.    
SCSS has the same directive, but it works a little differently. In CSS, your various CSS files are *all* sent to the browser as separate files,   
and the browser then stitches them together before reading and executing the rules.   
Because SCSS is a *pre*processor, the language is compiled down to CSS *before* it is sent to the browser.     
This means that any `@import` rules will actually import those SCSS rules *into the file you are importing into*, making one single CSS file to be served to the browser.    
SCSS takes advantage of this to give you something called a 'partial'.  
A partial file is a file that will *only ever be imported into your main CSS file*. That file will never be compiled on it's own, it *has* to be imported first.   
partial files begin with an underscore(\_).    
An example of a well-structured SCSS file tree could look something like this:   

- App 
	- index.html 
	- styles.css 
	- Assets
		- images
		- styles 
			- main.scss
			- partials 
				- \_variables.scss
				- \_mixins.scss
				- \_grid.scss

So let's have a look at how those partial files will find their way into the main.scss file:  

```
main.scss:  

@import 'partials/variables';
@import 'partials/mxins';
@import 'partials/grid';
```
You can use imports and partials to help you better organize your code, and to use snippets (or *partials* ;)) wherever you need.  

- - - -

### Mixins   
A mixin allows you to make a group of CSS declarations that you can use throughout your site.    
For example, defining an element as a flex container with `justify-content: center;` and `align-items: center` is a good way to vertically and horizontally center an item,    
but retyping that declaration over and over can get tedious. Mixins to the rescue!:   

```
@mixin flexCenter {
	display: flex;
	justify-content: center;
	align-items: center;
}
```

To declare a mixin, we use the '@' decorator, followed by the keyword `mixin`, the name of the mixin, and a set of curly braces to hold our declarations.     
To use the mixin, we simply `@include` followed by the name, like so:    

```
.box {
	@include flexCenter;
}
```   
([CodePen](https://codepen.io/Mike8327/pen/EpdxNj).)

we can also pass parameters to our mixins:  

HTML: 
```
<div class="hideShowThis">  
  <div class="box">
    Another box huh?
  </div>
</div>
```

SCSS: 
```
$hideBox: true;

@mxin hideShowBox($hideBox) {
	@if $hideBox {
		display: none;
	} @else {
		display: block;
	}
}

.hideShowThis {
  @include hideShowBox($hideBox);
}
```
([CodePen](https://codepen.io/Mike8327/pen/LBgYzx).)   

- - - -

### [Control Directives](http://thesassway.com/intermediate/if-for-each-while)
Above, we used the `@if` keyword inside of our mixin. Let's talk about that for a bit.
SASS control directives provide flow and logic and give you a finite level of decision making required by mixins and functions.   
There are a number of control directives available in SASS, along with scripts that will run when those directives are called (SassScripts, we'll talk about these another time).     
Control directives are a bit of a rabbit hole unto themselves, so we'll just look at the `@if` directive quickly and move on, but note that there are *three* others: 

 - `@for` to apply styles to a number of elements (eg: children of a `<ul>` tag); 
 - `@each` // TODO: Find an explanation;
 - `@while` // ^^  
 

#### `@if`: 
`@if` is a useful directive that will check the truthiness of a variable, and run some code if it's true, just like any other if statement. Here'a a fairly simple example: 

```
$lightTheme: true !default;

.box {
	@if($lightTheme) {
		background-color: white;
	} @else {
		background-color: black
	} 
}
```
([CodePen](https://codepen.io/Mike8327/pen/ajRzoW).)

This is an interesting and useful little tool, but it does come with some limitations. 
Because SASS is a preprocessor, it compiles down to CSS *before* runtime.    
That means that by the time you hit a live screen, all your SCSS is now CSS and your SCSS variables can no longer be changed.  

- - - - 

### Inheritance and extending
One of the core values behind a preprocessor is to make your code neater, cleaner, and easier to read and interact with.    
One of the key principles used to achieve this is called DRY: 
#### DON'T REPEAT YOURSELF   
DRY is a principle used all over the programming world, and describes the action of writing your code to be as re-usable as possible, to avoid things like writing the same function 5 times in 4 places.       
In SCSS, we have the `@extend` keyword. This is used much like extending in any other language:   

```
%message-shared {
    border: 1px solid;
    border-radius: 5px;
    width: 5rem;
    text-align: center;
    padding: .5rem;
}

.message {
	@extend %message-shared;
}

.error {
	border-color: red;
	color: red;
}

.success {
	border-color: green;
	color: green;
}

.warning {
	border-color: orange;
	color: orange;
}
```
([CodePen](https://codepen.io/Mike8327/pen/wxYBGL).)

Note that we use the `%` symbol to denote an extendable set of conditions, but we can technically extend *any* set of CSS rules.    
The above is a simple example of how you could use the extended a class to set a variety of styles once, while adding finer detail for individual use-cases. 

- - - -

### Operators
In CSS, there's a function available called `calc()`, which allows you to run small calculations inside the brackets: 

```
.box {
	width: calc(2rem + 20%);
}
```

In SCSS, we can do the same thing without needing to call the function: 

```
.box {
	width: 2 + 20%;
}
```
([CodePen](https://codepen.io/Mike8327/pen/zLmxEE).)

In Scss, all of the mathematical operators (`+`, `-`, `/`, `*`, and `%` ) are available for use anywhere. 