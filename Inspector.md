## The Inspector:
I'm sure we've all used the Javascript console in the Chrome Dev Tools. But not many devs make use of the Inspector. The inspector is a tool that gives you access to a view of the current HTML DOM Tree, along with all the styles that apply to those elements. 

More than that though, the inspector allows to actually edit the html and CSS in the DOM tree. It's super useful for debugging UI problems, specially in Angular where it takes a little while to recompile your changes after every save. The list below gives you a breakdown of what you can do with the Inspector, along with links to the official Chrome Dev Tools docs explaining how it all works: 

- (cmd || ctrl) + shift + c to open the inspector. 
- [The Inspector can be used to live-edit css and html on the fly.](https://developers.google.com/web/tools/chrome-devtools/css/) 
- You can view and edit values of existing classes, add new classes or add style inline.   
- You can view and edit style attached to pseudo-selectors (:hover, :focused etc)
- You can examine and edit the box model on any element.
- You can view and re-order the HTML DOM tree
- [You can view, edit, and add html in the DOM tree.](https://developers.google.com/web/tools/chrome-devtools/inspect-styles/) 
- You can view inherited styles and see the stylesheet the rules are defined in. 
- [You can view and edit animations.](https://developers.google.com/web/tools/chrome-devtools/inspect-styles/animations)
- [You can step through HTML DOM-breakpoints](https://developers.google.com/web/tools/chrome-devtools/inspect-styles/edit-dom#set_dom_breakpoints)