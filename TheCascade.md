## The Cascade: 
The cascade is an algorithm that defines how to combine property values originating from different sources.   
It lies at the core of CSS, as emphasized by the name: Cascading Style Sheets.
The CSS cascade algorithm's job is to select CSS declarations in order to determine the correct values for CSS properties.   
CSS declarations originate from different origins: 

- the user-agent style sheet    
- the author style sheet   
- and the user style sheet   

The algorithm determines how to find the value to apply to a given element:     

- First It first filters all the rules from the different sources to keep only the rules that apply to a given element. That means rules whose selector matches the given element. 
- Then it sorts these rules according to their importance, that is, whether or not they are followed by !important, and by their origin.   

The cascade is in ascending order, which means that !important values from a user-defined style sheet have precedence   
over normal values originated from a user-agent style sheet. 

Origin  | Importance
-------------: | :-------------
User agent  | Normal
User  | Normal
author  | Normal
Author  | !important
User  | !important
User agent  | !important

So the order in which the cascade will run is:  

1. Importance   
2. Specificity   
3. Source Order 

- - - -

### The `!important` tag: 
In CSS, there is a special piece of syntax you can use to make sure that a certain declaration will always win over all others: `!important.` Let's take a look:      

The Html:
```
<div class="box">
	<div class="thing thing-one">
		<p>A Thing</p>
	</div>
	<div class="thing thing-two">
		<p>Another thing</p>
	</div>
</div>
``` 

The CSS:
```
.box {
    border: 5px solid red;
    background-color: black;
    padding: 1rem;
}

.thing-one {
    border: none !important;
    background-color: green !important;
    color: lightgrey !important;
}

.thing {
    border: 5px solid white;
    background-color: blue;
    font-size: 14 px;
    color: white;
    padding: .5rem;
    margin: 15px;
}
```
([CodePen](https://codepen.io/Mike8327/pen/LBJwJg)).    

This code snippet will produce a box with a red border and black background.   
Inside the box will be two *things*, one with a white border and blue background, the other with no border and a green background.   
The color or the text in the second thing will be white, but in the first it will be light grey.    

The `!important` tag on the `border` and `background-color` of `.thing-one` will both override the style set on them in `.thing`. The only way to then override the `!important`   
tag is to add the overriding style *inline*, or add more specificity with an `!important` tag:     

The Html:
```
<div class="box">
	<div class="thing thing-one">
		<p>A Thing</p>
	</div>
	<div class="thing thing-two">
		<p>Another thing</p>
	</div>
</div>
``` 

The CSS:
```
.box {
	border: 5px solid red;
	background-color: black;
}

.thing-two p {
	color: white;
}

p {
	color: green;
}
```   
([CodePen](https://codepen.io/Mike8327/pen/zLJgyW)).    

In this case, the color of the `<p>` tag is being set to green, but the specificity in the rule for thing-two (`.thing-two p`) will override that rule as it is more specific. 

Specificity and `!important` can be very powerful tools in ordering and structuring your css documents.    
`!important` should be used very sparingly when trying to override other styles, as it can lead to specificity problems later on, making it more difficult to style your html.    
There are times, however, where `!important` may come in handy. For example, utility classes (buttons, boxes etc) that should have   
a consistent style throughout the app can be given `!important` tags to ensure they are never overridden. ([ref](https://css-tricks.com/when-using-important-is-the-right-choice/))## The Cascade: 
                                                                                                                                                                                   The cascade is an algorithm that defines how to combine property values originating from different sources.   
                                                                                                                                                                                   It lies at the core of CSS, as emphasized by the name: Cascading Style Sheets.
                                                                                                                                                                                   The CSS cascade algorithm's job is to select CSS declarations in order to determine the correct values for CSS properties.   
                                                                                                                                                                                   CSS declarations originate from different origins: 
                                                                                                                                                                                   
                                                                                                                                                                                   - the user-agent style sheet    
                                                                                                                                                                                   - the author style sheet   
                                                                                                                                                                                   - and the user style sheet   
                                                                                                                                                                                   
                                                                                                                                                                                   The algorithm determines how to find the value to apply to a given element:     
                                                                                                                                                                                   
                                                                                                                                                                                   - First It first filters all the rules from the different sources to keep only the rules that apply to a given element. That means rules whose selector matches the given element. 
                                                                                                                                                                                   - Then it sorts these rules according to their importance, that is, whether or not they are followed by !important, and by their origin.   
                                                                                                                                                                                   
                                                                                                                                                                                   The cascade is in ascending order, which means that !important values from a user-defined style sheet have precedence   
                                                                                                                                                                                   over normal values originated from a user-agent style sheet. 
                                                                                                                                                                                   
                                                                                                                                                                                   Origin  | Importance
                                                                                                                                                                                   -------------: | :-------------
                                                                                                                                                                                   User agent  | Normal
                                                                                                                                                                                   User  | Normal
                                                                                                                                                                                   author  | Normal
                                                                                                                                                                                   Author  | !important
                                                                                                                                                                                   User  | !important
                                                                                                                                                                                   User agent  | !important
                                                                                                                                                                                   
                                                                                                                                                                                   So the order in which the cascade will run is:  
                                                                                                                                                                                   
                                                                                                                                                                                   1. Importance   
                                                                                                                                                                                   2. Specificity   
                                                                                                                                                                                   3. Source Order 
                                                                                                                                                                                   
                                                                                                                                                                                   - - - -
                                                                                                                                                                                   
                                                                                                                                                                                   ### The `!important` tag: 
                                                                                                                                                                                   In CSS, there is a special piece of syntax you can use to make sure that a certain declaration will always win over all others: `!important.` Let's take a look:      
                                                                                                                                                                                   
                                                                                                                                                                                   The Html:
                                                                                                                                                                                   ```
                                                                                                                                                                                   <div class="box">
                                                                                                                                                                                   	<div class="thing thing-one">
                                                                                                                                                                                   		<p>A Thing</p>
                                                                                                                                                                                   	</div>
                                                                                                                                                                                   	<div class="thing thing-two">
                                                                                                                                                                                   		<p>Another thing</p>
                                                                                                                                                                                   	</div>
                                                                                                                                                                                   </div>
                                                                                                                                                                                   ``` 
                                                                                                                                                                                   
                                                                                                                                                                                   The CSS:
                                                                                                                                                                                   ```
                                                                                                                                                                                   .box {
                                                                                                                                                                                       border: 5px solid red;
                                                                                                                                                                                       background-color: black;
                                                                                                                                                                                       padding: 1rem;
                                                                                                                                                                                   }
                                                                                                                                                                                   
                                                                                                                                                                                   .thing-one {
                                                                                                                                                                                       border: none !important;
                                                                                                                                                                                       background-color: green !important;
                                                                                                                                                                                       color: lightgrey !important;
                                                                                                                                                                                   }
                                                                                                                                                                                   
                                                                                                                                                                                   .thing {
                                                                                                                                                                                       border: 5px solid white;
                                                                                                                                                                                       background-color: blue;
                                                                                                                                                                                       font-size: 14 px;
                                                                                                                                                                                       color: white;
                                                                                                                                                                                       padding: .5rem;
                                                                                                                                                                                       margin: 15px;
                                                                                                                                                                                   }
                                                                                                                                                                                   ```
                                                                                                                                                                                   ([CodePen](https://codepen.io/Mike8327/pen/LBJwJg)).    
                                                                                                                                                                                   
                                                                                                                                                                                   This code snippet will produce a box with a red border and black background.   
                                                                                                                                                                                   Inside the box will be two *things*, one with a white border and blue background, the other with no border and a green background.   
                                                                                                                                                                                   The color or the text in the second thing will be white, but in the first it will be light grey.    
                                                                                                                                                                                   
                                                                                                                                                                                   The `!important` tag on the `border` and `background-color` of `.thing-one` will both override the style set on them in `.thing`. The only way to then override the `!important`   
                                                                                                                                                                                   tag is to add the overriding style *inline*, or add more specificity with an `!important` tag:     
                                                                                                                                                                                   
                                                                                                                                                                                   The Html:
                                                                                                                                                                                   ```
                                                                                                                                                                                   <div class="box">
                                                                                                                                                                                   	<div class="thing thing-one">
                                                                                                                                                                                   		<p>A Thing</p>
                                                                                                                                                                                   	</div>
                                                                                                                                                                                   	<div class="thing thing-two">
                                                                                                                                                                                   		<p>Another thing</p>
                                                                                                                                                                                   	</div>
                                                                                                                                                                                   </div>
                                                                                                                                                                                   ``` 
                                                                                                                                                                                   
                                                                                                                                                                                   The CSS:
                                                                                                                                                                                   ```
                                                                                                                                                                                   .box {
                                                                                                                                                                                   	border: 5px solid red;
                                                                                                                                                                                   	background-color: black;
                                                                                                                                                                                   }
                                                                                                                                                                                   
                                                                                                                                                                                   .thing-two p {
                                                                                                                                                                                   	color: white;
                                                                                                                                                                                   }
                                                                                                                                                                                   
                                                                                                                                                                                   p {
                                                                                                                                                                                   	color: green;
                                                                                                                                                                                   }
                                                                                                                                                                                   ```   
                                                                                                                                                                                   ([CodePen](https://codepen.io/Mike8327/pen/zLJgyW)).    
                                                                                                                                                                                   
                                                                                                                                                                                   In this case, the color of the `<p>` tag is being set to green, but the specificity in the rule for thing-two (`.thing-two p`) will override that rule as it is more specific. 
                                                                                                                                                                                   
                                                                                                                                                                                   Specificity and `!important` can be very powerful tools in ordering and structuring your css documents.    
                                                                                                                                                                                   `!important` should be used very sparingly when trying to override other styles, as it can lead to specificity problems later on, making it more difficult to style your html.    
                                                                                                                                                                                   There are times, however, where `!important` may come in handy. For example, utility classes (buttons, boxes etc) that should have   
                                                                                                                                                                                   a consistent style throughout the app can be given `!important` tags to ensure they are never overridden. ([ref](https://css-tricks.com/when-using-important-is-the-right-choice/))