## CSS Frameworks: 
Styling an entire website or app from the ground up can be quite a difficult task, specially if you're the only person building. Luckily, we have CSS Frameworks to help us out.   
So what exactly is a CSS Framework? In simplicity, it's a bunch of pre-styled CSS classes that you can use throughout your app. Of course, we all know it's a little more complicated than this.    
Most of the bigger CSS Frameworks also give you functionality (Modals, dropdowns, pop-overs), help you control responsiveness (how well your elements handle different screen sizes), and cross-browser support (the best part, imo).    
There are *plenty* of CSS Frameworks out there, so how do you choose one? Oftentimes, when starting a new project, this will be one of the first questions we ask. This is a difficult question to answer though, because it really depends of the project itself, as well what the developer has experience with.   
If we're working in Angular (which we mostly are), then there are a few more considerations to take into account:   
Angular doesn't like it when we manipulate the DOM directly. It keeps its own copy of the DOM that it uses to run its famous change detection (a lesson for another time). If we ignore this and manipulate the DOM directly, then Angular's change detection is going to start failing and producing some unwanted results.    

To overcome this, we have two options:  

- We can find a version of our chosen framework that has been wrapped for Angular (this is typically in the form of a series of styled Angular components that you download and call).    
- Or we can use a CSS-Only Framework, that doesn't contain Jquery (or any javascript at all). This does mean, however, that we will have to implement the functionality ourselves.

So let's have a look at some of our options: 

- - - - 
### [Bootstrap](https://getbootstrap.com/docs/4.1/getting-started/introduction/) and [NG-Bootstrap](https://ng-bootstrap.github.io/#/home) 
Bootstrap is the Framework we use in Mabble. And NG-Bootstrap is how we implement the functional classes. In this case, we still import Bootstrap itself into the app for the styled classes, but instead of using Jquery and Popper.js for the functionality, we import the styled components available from NG-Bootstrap.    

Bootstrap is a super useful framework, and really stands out when it comes to its [utility classes](https://getbootstrap.com/docs/4.1/utilities/borders/). 

### [Material Design](https://material.io/) and [Angular Material](https://material.angular.io/) 
Google's Material Design has been around probably the longest. It's not really a framework though, it's actually a design specification built upon UI and UX best practices. It's as much a psychological and scientific document as it is a technical document. However, plenty of Frameworks have been created from it, and the one that we are particularly interested in is Angular Material.    

Angular Material is a very opinionated framework, making life difficult for those that try to design outside of it's specifications (for example, it takes a hack workaround to properly change the background color to anything other than white or grey). Having said that however, Angular Material is definitely one of the more visually appealing frameworks on the list, and it offers a series of animations on almost all of its components that's just about unrivalled. Definitely one to take a look at (In fact, if you pay attention next time you're looking something up on the [Angular Docs](https://angular.io/docs), you might just notice that they themselves have styled their app with Angular Material.)  

### [PrimeNG](https://www.primefaces.org/primeng/) 
Primefaces' PrimeNG is a set of styled components built specifically for Angular, rather than adapted from a more generic framework.   
For this reason, it integrates better with Angular than many of the others, and comes with some seriously cool UI components baked in (Things like Drag and Drop, Charts, File uploader, and a bunch more).   

### [Bulma](https://bulma.io/documentation/) 
Bulma is one of those *CSS-Only* frameworks I was talking about earlier. It doesn't come with *any* Javascript, which makes it absolutely safe to use with Angular.   
Bulma offers quite a lot in the way of UI components, and a lot of display mechanisms (`.box`, `.container`, `.card`, `.tile`, etc).  
It also offers an extremely easy-to-read syntax, making it easier for other developers to figure out what's going on. 

([An example of a basic Navbar built with Bulma](https://codepen.io/Mike8327/pen/MBPzYz).)

Using the functionality of the bulma classes is generally fairly easy, for example to display a modal on click:  

HTML:
```
<div class="card has-text-centered">
  <div class="card-content">
    <button (click)="showModal = true" class="button is-small is-outlined is-info">
      Open Modal
    </button>
  </div>
</div>

<div class="modal" [class.is-active] = "showModal">
  <div class="modal-background"></div>
  <div class="modal-content">
    <div class="box has-text-centered">
      Hello, I am a box... In a modal!  
    </div>
  </div>
  <button (click)="showModal = false" class="modal-close is-large"></button>
</div>
```

TS:
```
showModal = false;
```
([CodePen](https://codepen.io/Mike8327/pen/gjBZey) (Note: Angular doesn't run in CodePen, so the example uses javascript instead).)
   