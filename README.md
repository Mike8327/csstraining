# CSS Training

## Welcome to CSS Training. 
- - - -
## Topics: 

1. The Inspector: 
	- [Inspecting and changing CSS](https://developers.google.com/web/tools/chrome-devtools/css/)
	- [CSS Reference](https://developers.google.com/web/tools/chrome-devtools/css/reference)
	- [Inspecting and Changing HTML](https://developers.google.com/web/tools/chrome-devtools/inspect-styles/)
	- [Inspecting Animations](https://developers.google.com/web/tools/chrome-devtools/inspect-styles/animations)	
	- [Pure CSS Francine](http://diana-adrianne.com/purecss-francine/)
2. The Cascade: 
 	- [What is the Cascade?](https://developer.mozilla.org/en-US/docs/Web/CSS/Cascade)
	- [specificity and inheritance](https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Cascade_and_inheritance)
	- [The `!important` tag and when to use it](https://css-tricks.com/when-using-important-is-the-right-choice/)
3. PreProcessors: 
	- What is a CSS Preprocessor? 
		- [SASS/SCSS](http://sass-lang.com/guide)(The one we use)
		- [LESS](http://lesscss.org/#overview)
		- [Stylus](http://stylus-lang.com/)
		- [PostCSS](https://postcss.org/)
4. [Layout and Positioning](https://www.smashingmagazine.com/2018/08/flexbox-display-flex-container/): 
    - [Flexbox and flex-containers](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
		- [FlexboxFroggy](https://flexboxfroggy.com/)
		- [Flex Zombies](https://mastery.games/p/flexbox-zombies)
    - [CSS-Grid](https://css-tricks.com/snippets/css/complete-guide-grid/)
		- [CSS-Grid Garden](https://cssgridgarden.com/)
5. CSS Frameworks: 
	- [Bootstrap](https://getbootstrap.com/) and [ng-bootstrap](https://ng-bootstrap.github.io/#/home)
	- [Material Design](https://material.io/) and [Angular Material](https://material.angular.io/)
	- [PrimeNG](https://www.primefaces.org/primeng/#/)
	- CSS-Only: 
		- [Bulma](https://bulma.io/)
		- [PicnicCSS](https://picnicss.com/)
		- [PureCSS](https://purecss.io/)
	- [CSS-Reset](https://meyerweb.com/eric/tools/css/reset/): 
		- [Eric Meyer's 'CSS Reset 2.0'](https://cssreset.com/scripts/eric-meyer-reset-css/)

- References: 
	- [CSS-Tricks.com](https://css-tricks.com)
	- [Mozilla Developer docs](https://developer.mozilla.org/en-US/docs/Learn/CSS)
    - [MarkDown](https://daringfireball.net/projects/markdown/)
	- [Reddit](https://reddit.com):
		- [CSS](https://reddit.com/r/css)
		- [WebDev](https://reddit.com/r/webdev)
		- [Angular(first subreddit)](https://reddit.com/r/angular)
		- [Angular(second subreddit)](https://reddit.com/r/angular2) 

- - - -
- - - -

## The Inspector:
I'm sure we've all used the Javascript console in the Chrome Dev Tools. But not many devs make use of the Inspector. The inspector is a tool that gives you access to a view of the current HTML DOM Tree, along with all the styles that apply to those elements. 

More than that though, the inspector allows to actually edit the html and CSS in the DOM tree. It's super useful for debugging UI problems, specially in Angular where it takes a little while to recompile your changes after every save. The list below gives you a breakdown of what you can do with the Inspector, along with links to the official Chrome Dev Tools docs explaining how it all works: 

- (cmd || ctrl) + shift + c to open the inspector. 
- [The Inspector can be used to live-edit css and html on the fly.](https://developers.google.com/web/tools/chrome-devtools/css/) 
- You can view and edit values of existing classes, add new classes or add style inline.   
- You can view and edit style attached to pseudo-selectors (:hover, :focused etc)
- You can examine and edit the box model on any element.
- You can view and re-order the HTML DOM tree
- [You can view, edit, and add html in the DOM tree.](https://developers.google.com/web/tools/chrome-devtools/inspect-styles/) 
- You can view inherited styles and see the stylesheet the rules are defined in. 
- [You can view and edit animations.](https://developers.google.com/web/tools/chrome-devtools/inspect-styles/animations)
- [You can step through HTML DOM-breakpoints](https://developers.google.com/web/tools/chrome-devtools/inspect-styles/edit-dom#set_dom_breakpoints)

- - - -
- - - -

## The Cascade: 
The cascade is an algorithm that defines how to combine property values originating from different sources.   
It lies at the core of CSS, as emphasized by the name: Cascading Style Sheets.
The CSS cascade algorithm's job is to select CSS declarations in order to determine the correct values for CSS properties.   
CSS declarations originate from different origins: 

- the user-agent style sheet    
- the author style sheet   
- and the user style sheet   

The algorithm determines how to find the value to apply to a given element:     

- First It first filters all the rules from the different sources to keep only the rules that apply to a given element. That means rules whose selector matches the given element. 
- Then it sorts these rules according to their importance, that is, whether or not they are followed by !important, and by their origin.   

The cascade is in ascending order, which means that !important values from a user-defined style sheet have precedence   
over normal values originated from a user-agent style sheet. 

Origin  | Importance
-------------: | :-------------
User agent  | Normal
User  | Normal
author  | Normal
Author  | !important
User  | !important
User agent  | !important

So the order in which the cascade will run is:  

1. Importance   
2. Specificity   
3. Source Order 

- - - -

### The `!important` tag: 
In CSS, there is a special piece of syntax you can use to make sure that a certain declaration will always win over all others: `!important.` Let's take a look:      

The Html:
```
<div class="box">
	<div class="thing thing-one">
		<p>A Thing</p>
	</div>
	<div class="thing thing-two">
		<p>Another thing</p>
	</div>
</div>
``` 

The CSS:
```
.box {
    border: 5px solid red;
    background-color: black;
    padding: 1rem;
}

.thing-one {
    border: none !important;
    background-color: green !important;
    color: lightgrey !important;
}

.thing {
    border: 5px solid white;
    background-color: blue;
    font-size: 14 px;
    color: white;
    padding: .5rem;
    margin: 15px;
}
```
([CodePen](https://codepen.io/Mike8327/pen/LBJwJg)).    

This code snippet will produce a box with a red border and black background.   
Inside the box will be two *things*, one with a white border and blue background, the other with no border and a green background.   
The color or the text in the second thing will be white, but in the first it will be light grey.    

The `!important` tag on the `border` and `background-color` of `.thing-one` will both override the style set on them in `.thing`. The only way to then override the `!important`   
tag is to add the overriding style *inline*, or add more specificity with an `!important` tag:     

The Html:
```
<div class="box">
	<div class="thing thing-one">
		<p>A Thing</p>
	</div>
	<div class="thing thing-two">
		<p>Another thing</p>
	</div>
</div>
``` 

The CSS:
```
.box {
	border: 5px solid red;
	background-color: black;
}

.thing-two p {
	color: white;
}

p {
	color: green;
}
```   
([CodePen](https://codepen.io/Mike8327/pen/zLJgyW)).    

In this case, the color of the `<p>` tag is being set to green, but the specificity in the rule for thing-two (`.thing-two p`) will override that rule as it is more specific. 

Specificity and `!important` can be very powerful tools in ordering and structuring your css documents.    
`!important` should be used very sparingly when trying to override other styles, as it can lead to specificity problems later on, making it more difficult to style your html.    
There are times, however, where `!important` may come in handy. For example, utility classes (buttons, boxes etc) that should have   
a consistent style throughout the app can be given `!important` tags to ensure they are never overridden. ([ref](https://css-tricks.com/when-using-important-is-the-right-choice/))

- - - -
- - - -

## PreProcessors: 
A preprocessor is a program that takes one type of data and converts it to another type of data (TypeScript is actually an example of what could be called a Javascript preprocessor, in that it takes one type of data (TypeScript code) and compiles it down to another (in this case Javascript)).   
In the case of css preprocessors, they compile down to css. There are four well-known css preprocessors:  

1. [SASS/SCSS](http://sass-lang.com/guide)
2. [LESS](http://lesscss.org/#overview)
3. [Stylus](http://stylus-lang.com/)
4. [PostCSS](https://postcss.org/)

The differences between the four are fairly minor (mostly to do with syntax differences), but it would be a good idea to get an overview of each. 
For now, we'll focus on SCSS (SCSS is the syntax we use, but it is a slightly newer syntax extension of SASS, and is governed by their rules). 

There are a few things that SCSS gives you access to over and above regular CSS expressions: 
- Variables
- Mixins
- Functions
- Conditionals
- Operators

- - - -

### Variables: 
SCSS Variables are ways to store CSS rules for re-use throughout your application. Variables are defined using the `$` symbol.    
A good example (and one very often used in the real-world) is a variables file for your app's colors:  
```
_variables.scss: 
// Scss color Variables

$primary: #4286f4; // Blue
$secondary: #d8d8d8; // Grey
$accent: #f75b1d; // Orange   
```

Now, whenever you want to use one of those colors, it's as easy as: 

```
@import '../assets/atyles/partials/variables'; // Note, you must import the variables partial (more on partials in a bit) in order to use them in that file.

div {
	color: $primary;
	background-color: $secondary;
	border: 1px solid $accent;
}
```
([CodePen](https://codepen.io/Mike8327/pen/ajRoVx).)

A quick note about css variables:    
Variables exist in regular css as well. They're a different beast to SCSS variables, and you can have both separately. They are defined a little different however:  

```
// CSS Variables are typically defined on the root element, oftentimes this is an alias for the html element. However, you can define them on any element you wish

:root {
	--primary: #4286f4; // Blue
	--secondary: #d8d8d8; // Grey
	--accent: #f75b1d; // Orange
}

div {
	color: var(--primary);
	background-color: var(--secondary);
	border: 1px solid var(--accent);
}
```
([CodePen](https://codepen.io/Mike8327/pen/vaVBrG).)

Note that variables (SCSS variables and CSS variables) can be *any* css rule. 

- - - -

### Nesting: 
In CSS, one of the ways to increase specificity of your code is to 'nest' it. For example: 

HTMl:
```
<section class="container">
	<div class="box">
		<ul class="menu">
			<li>Item 1</li>
			<li>Item 2</li>
			<li>Item 3</li>
			<li>Item 4</li>
		</ul>
	</div>
</section>
```

CSS:
```
.box {
	color: green
}

Versus

.container .box .menu li {
	color: green
}
```    
([CodePen](https://codepen.io/Mike8327/pen/zLmYGO).)

In the second example, the selectors have been nested to increase the specificity of the rule. In SCSS, we can nest like this: 

```
.container {
	display: flex;
	justfify-content: center;
	.box {
    width: 100%;
		border: 1px solid black;
		background-color: lightblue;
		.menu {
      		display: flex;
      		justify-content: space-around;
			list-style: none;
			li {
				color: green;
			}
		}
	}
}
```
([CodePen](https://codepen.io/Mike8327/pen/KBGKdr).)    

Nesting in SCSS allows for much neater code, making it far more readable for other developers coming after you.    
However, best-practice is SCSS says to never nest deeper than two levels *unless you specifically need to nest deeper for specificity*.    
This is because the above code will compile to the following: 

```
.container {
	display: flex;
	justfify-content: center;
}
.container .box {
	border: 1px solid black;
	background-color: lightblue;
}
.container .box .menu {
	list-style: none;
}
.container .box .menu li {
	color: green;
}
```

As you can see, each nested selector compiles to nested CSS. This means that it can be very easy to accidentally create major specificity problems in your CSS. 

- - - -

### Partials and imports  
In CSS, you can use a directive called `@import` to import another CSS file into your own. This can be useful to help with modulating your CSS for better readability   
and easier maintenance.    
SCSS has the same directive, but it works a little differently. In CSS, your various CSS files are *all* sent to the browser as separate files,   
and the browser then stitches them together before reading and executing the rules.   
Because SCSS is a *pre*processor, the language is compiled down to CSS *before* it is sent to the browser.     
This means that any `@import` rules will actually import those SCSS rules *into the file you are importing into*, making one single CSS file to be served to the browser.    
SCSS takes advantage of this to give you something called a 'partial'.  
A partial file is a file that will *only ever be imported into your main CSS file*. That file will never be compiled on it's own, it *has* to be imported first.   
partial files begin with an underscore(\_).    
An example of a well-structured SCSS file tree could look something like this:   

- App 
	- index.html 
	- styles.css 
	- Assets
		- images
		- styles 
			- main.scss
			- partials 
				- \_variables.scss
				- \_mixins.scss
				- \_grid.scss

So let's have a look at how those partial files will find their way into the main.scss file:  

```
main.scss:  

@import 'partials/variables';
@import 'partials/mxins';
@import 'partials/grid';
```
You can use imports and partials to help you better organize your code, and to use snippets (or *partials* ;)) wherever you need.  

- - - -

### Mixins   
A mixin allows you to make a group of CSS declarations that you can use throughout your site.    
For example, defining an element as a flex container with `justify-content: center;` and `align-items: center` is a good way to vertically and horizontally center an item,    
but retyping that declaration over and over can get tedious. Mixins to the rescue!:   

```
@mixin flexCenter {
	display: flex;
	justify-content: center;
	align-items: center;
}
```

To declare a mixin, we use the '@' decorator, followed by the keyword `mixin`, the name of the mixin, and a set of curly braces to hold our declarations.     
To use the mixin, we simply `@include` followed by the name, like so:    

```
.box {
	@include flexCenter;
}
```   
([CodePen](https://codepen.io/Mike8327/pen/EpdxNj).)

we can also pass parameters to our mixins:  

HTML: 
```
<div class="hideShowThis">  
  <div class="box">
    Another box huh?
  </div>
</div>
```

SCSS: 
```
$hideBox: true;

@mxin hideShowBox($hideBox) {
	@if $hideBox {
		display: none;
	} @else {
		display: block;
	}
}

.hideShowThis {
  @include hideShowBox($hideBox);
}
```
([CodePen](https://codepen.io/Mike8327/pen/LBgYzx).)   

- - - -

### [Control Directives](http://thesassway.com/intermediate/if-for-each-while)
Above, we used the `@if` keyword inside of our mixin. Let's talk about that for a bit.
SASS control directives provide flow and logic and give you a finite level of decision making required by mixins and functions.   
There are a number of control directives available in SASS, along with scripts that will run when those directives are called (SassScripts, we'll talk about these another time).     
Control directives are a bit of a rabbit hole unto themselves, so we'll just look at the `@if` directive quickly and move on, but note that there are *three* others: 

 - `@for` to apply styles to a number of elements (eg: children of a `<ul>` tag); 
 - `@each` // TODO: Find an explanation;
 - `@while` // ^^  
 

#### `@if`: 
`@if` is a useful directive that will check the truthiness of a variable, and run some code if it's true, just like any other if statement. Here'a a fairly simple example: 

```
$lightTheme: true !default;

.box {
	@if($lightTheme) {
		background-color: white;
	} @else {
		background-color: black
	} 
}
```
([CodePen](https://codepen.io/Mike8327/pen/ajRzoW).)

This is an interesting and useful little tool, but it does come with some limitations. 
Because SASS is a preprocessor, it compiles down to CSS *before* runtime.    
That means that by the time you hit a live screen, all your SCSS is now CSS and your SCSS variables can no longer be changed.  

- - - - 

### Inheritance and extending
One of the core values behind a preprocessor is to make your code neater, cleaner, and easier to read and interact with.    
One of the key principles used to achieve this is called DRY: 
#### DON'T REPEAT YOURSELF   
DRY is a principle used all over the programming world, and describes the action of writing your code to be as re-usable as possible, to avoid things like writing the same function 5 times in 4 places.       
In SCSS, we have the `@extend` keyword. This is used much like extending in any other language:   

```
%message-shared {
    border: 1px solid;
    border-radius: 5px;
    width: 5rem;
    text-align: center;
    padding: .5rem;
}

.message {
	@extend %message-shared;
}

.error {
	border-color: red;
	color: red;
}

.success {
	border-color: green;
	color: green;
}

.warning {
	border-color: orange;
	color: orange;
}
```
([CodePen](https://codepen.io/Mike8327/pen/wxYBGL).)

Note that we use the `%` symbol to denote an extendable set of conditions, but we can technically extend *any* set of CSS rules.    
The above is a simple example of how you could use the extended a class to set a variety of styles once, while adding finer detail for individual use-cases. 

- - - -

### Operators
In CSS, there's a function available called `calc()`, which allows you to run small calculations inside the brackets: 

```
.box {
	width: calc(2rem + 20%);
}
```

In SCSS, we can do the same thing without needing to call the function: 

```
.box {
	width: 2 + 20%;
}
```
([CodePen](https://codepen.io/Mike8327/pen/zLmxEE).)

In Scss, all of the mathematical operators (`+`, `-`, `/`, `*`, and `%` ) are available for use anywhere. 

- - - - 
- - - -     

## Layout and Positioning
### FlexBox
Alright, let's talk about layout and positioning. 
One of the most commonly asked questions in css/html forums is:    
`I need to center this div/box/sentence horizontally and vertically, but it isn't working. Anyone know how to do it?`     
and the answer every time?    
`Use Flexbox`    

So what is Flexbox? We won't go into too much detail here, but it is part of the CSS spec and has [almost full cross-browser support](https://caniuse.com/#search=flex) (Quick note, that website, [caniuse.com/](https://caniuse.com/), is an absolute godsend in determining cross-browser support). Of course, IE is the one that only has partial support, but the bugs are minor and there are plenty of workarounds for the few edge-cases.    

Flex-box allows us to define a flex-container that will position *it's direct children* according to various rules you can set. There is a hell of a lot to know about flex-box, so I'm going to cover the basics and leave the more advanced stuff for... others...   

So let's start with the flex-container. 
Consider the following:    

```
<section class="site-header">
	<img src="path/to/image" class="site-logo"/>
	<ul class="navbar">
		<li class="link link-one link-active">
			Link One
		</li>
		<li class="link link-two">
			Link Two
		</li>
		<li class="link link-three">
			Link Three
		</li>
		<li class="link link-four">
			Link Four
		</li>
	</ul>
	<button class="github-button">
		<a href="https://github.com/user">Github</a> 
	</button>
</section>
```

Here we have a basic header containing a logo, a navbar, and a button that links to the project on github. Pretty standard stuff. We can use flex to easily lay those items out in a logical way. Before we start, take a look at the DOM tree there, notice how we have three levels: the parent container, the three separate elements of that container, and then specific children of those elements. This is important because a flex container applies it's rules only to direct children of itself. Let's see the CSS:    

```
.site-header {
	display: flex;
	justify-content: space-between;
	align-items: center;
}
```
So far, so good. Those three simple rules will take our three children and align them horizontally (`display: flex;(\*)`) and vertically (`align-items: center`) and then position them evenly on the horizontal axis using the space *between* (`justify-content: space-between`) them. However, because flex-containers only apply to direct children, that navbar of ours is still going to be displaying vertically. Good thing we can nest flex-containers!
 
(\* Actually, `display: flex;` isn't what's causing the horizontal alignment. `display: flex;` just creates the flex container, and one of the rules of a flex container is `flex-direction`, which is `row` by default. You'll see an example of this later on.)

```
.site-header {
	display: flex;
	justify-content: space-between;
    align-items: center;
}

.navbar {
	display: flex;
	justify-content: space-around;
}
```
([CodePen](https://codepen.io/Mike8327/pen/RBYqYy))

And there we go. We've defined our `<ul>` as a flex-container, and added `justify-content: space-around` to make sure that the space is distributed evenly *around* each item. 
This is a rather simple example, but you can appreciate the idea.    

Flex can be used to create almost any one-dimensional layout you can imagine. Let's look at another example quick:   

```
<section class="site-header-hero">
	<div class="hero-top">
		<img src="path/to/image" class="logo">
		<nav class="navbar">
			<a class="link link-one">
				Link One
			</a>
			<a class="link link-two">
				Link Two
			</a>
			<a class="link link-three">
				Link Three
			</a>
		</nav>
	</div>
	<div class="hero-middle">
		<h1 class="site-title">Welcome to ProjectCSS</h1>
		<button class="call-to-action"><a href="https://github.com/user">Check us out on Github!</a></button>
	</div>
	<div class="hero-bottom">
		<div class="related-links">
			<a class="r-link link-1">Link 1</a>
			<a class="r-link link-2">Link 2</a>
			<a class="r-link link-3">Link 3</a>
		</div>
	</div>
</section>
```

Ok, so that's a bit more than before. Here, we're going for a large Hero section, with a logo and navbar along the top, the site title and call-to-action in the middle, and some related links along the bottom. Let's see how flexbox can help us:   

```
.site-header-hero {
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
}

.hero-top {
  display: flex;
  justify-content: space-between;
}

.navbar {
  display: flex;
  justify-content: space-between;
}

.hero-middle {
  display: flex;
  flex-direction: column;
}

.related-links {
  display: flex;
  justify-content: space-around;
}
```
([CodePen](https://codepen.io/Mike8327/pen/VBGRgQ).)

The first thing to note is the `flex-direction` property on `.site-header-hero` and on `.hero-middle`. By default, that property is set to `row`, here we set it to `column` so that it lays out its children vertically. Also note that when using `flex-direction: column`, your axes become switched, so `justify-content` will align your items vertically, and `align-items` will align your content horizontally.    

There is *a lot* more to talk about when it comes to flex. Instead of typing it all out however, I'm going to pass it off to some other people who have done a way better job with this lesson than I ever could:  

- [Flexbox Froggy](https://flexboxfroggy.com/) is a little browser game to teach you the basics of flex. Help the frog get to his lily-pad using flex commands.  
- When you've finished helping old froggy get home, go test what you've learned with [Flexbox Zombies](https://mastery.games/p/flexbox-zombies). You'll have to create an account and login to get to the game, but it's 12 chapters, each with a few theory lessons and 7 or 8 exercises to teach you everything there is to know about flexbox. (I've only gotten 60% of the way through that one, it's not a short game).   

- - - - 

### CSS-Grid
Flexbox is great for one-dimensional layouts, and sometimes for two-dimensional layouts. But also not really. That sounds confusing but here's the thing: 
In Flexbox, **_The content dictates the layout_**. This isn't very noticeable in small projects or on fast internet connections, but when you use flexbox to layout an entire page, you will find shifting content as the page loads on slower connections. This is because flex-items grow and shrink to fit their containers. [This is a better explanation than any I could give](https://www.smashingmagazine.com/2018/08/flexbox-display-flex-container/)  

In CSS-Grid, **_The Container dictates the layout_** (to a degree). You see, when you define a CSS-Grid, you actively define the size and layout of the page. 

Before we continue, we should just note quickly that according to [CanIUse](https://caniuse.com/#search=css%20grid%20layout), grid is almost at full support (IE, you bastard). 

Let me give you an example. Consider the following HTML:

```
<section class="page">
	<header class="header">
		Header
	</header>
	<aside class="sidebar">
		Sidebar
	</aside>
	<div class="content">
		Content
	</div>
	<footer class="footer">
		Footer
	</footer>
</section>
```
What we have there is a basic page layout defining a header, a sidebar, a content area, and a footer. Creating that layout with flex would be possible, but it would involve a lot of work, and when you started filling it with content it would become difficult to keep track of which flex rules are impacting which children. let's see how CSS-Grid handles this:   

```
.page {
	display: grid;
	grid-template-columns: 360px 360px 360px 360px;
	grid-template-rows: 200px 200px 200px 200px 200px;
	grid-gap: 2px;
}
```
Alright cool, we've defined a grid that'll have 4 columns of 360px each, and 6 rows of 200px each. Before we carry on, let's just discuss how horrifically hard-coded this is. I've done it like this on purpose to show off exactly how explicit and strict you can be with your grid, even though most of the time you won't want to. Also, note how much text there is. If I wanted to create a grid the 50 columns, would I have to write out each column?     
Well no, there are actually a ton of abstractions around this. Your first options is the CSS `repeat()` function. This functions takes 2 arguments: the number of times you want to repeat, and the item that you want to repeat. So knowing this, our example up top might look something like this:   

```
.page {
	display: grid;
	grid-template-columns: repeat(4, 25%);
	grid-template-rows: repeat(6, 200px);
	grid-gap: .3rem;
}
```

Alright, straight off the bat we can see the `repeat()` function in action, separating our columns into 4 of 25% width, and rows into 6 of 200px height. This is better, but still not ideal.     
This might be the best time to have a quick sidebar about size units in CSS. We all know what a `px` is (that's one pixel on the screen). We know that percent refers to the percentage of the screen, and we've all used `rem` even if we're not 100% sure why it's better than `px`. You might also have heard of `em`, `vh`, `vw`, `vmin`, `fr`, `pt`, `pc`, `cm`, `mm`, `ex`, `in` and holy hell let's just stop already. As you can see, there are plenty of ways to size your elements with CSS. So which way is the best way? Well, that's a tough one to answer.    
- `rem` units are measured against the font-size of your root element (in most cases that's your `<html>` tag).
- `em` units are measured against the font-size of the direct parent element. 
- `vh` and `vw` measures 1/100th of the viewport height and width respectively (very useful for responsive design if used carefully). `vmin` measures against whichever of the other two are shortest. 
- `fr` measure a fraction of the space available. 


For now we'll leave it at these as they are the most common and useful. You can have a look [here](https://www.tutorialspoint.com/css/css_measurement_units.htm) to see what all the others mean (and maybe even find a good use-case for one I haven't thought of yet).  

So, back to CSS-Grid. How can we make sure our grid is responsive and easy to type out? Let's have a look: 

```
.page {
	width: 100%; // To ensure that the columns take up the full width of the page
	display: grid;
	grid-template-columns: repeat(4, 1fr);
	grid-template-rows: repeat(6, 1fr);
	grid-gap: .3rem;
}
```
Alright, looking better. That `1fr` is telling the browser that each of the 4 columns must take up 1 fraction of the space available. This means that our grid-gap of .3rem will be able to fill up space without pushing the edge of our grid off the page.    
Finally, on to the rest of the grid: 

```
.header {
	grid-column-start: 1;
	grid-column-end: 5;
	grid-row-start: 0;
	grid-row-end: 1;
}

.sidebar {
	grid-column-start: 0;
	grid-column-end: 2;
	grid-row-start: 1;
	grid-row-end: 5;
}

.content {
	grid-column-start: 2;
	grid-column-end: 5;
	grid-row-start: 1;
	grid-row-end: 5;
}

.footer {
	grid-column-start: 1;
	grid-column-end: 5;
	grid-row-start: 5;
	grid-row-end: 6;
}
```
([CodePen](https://codepen.io/Mike8327/pen/MBqMRa))

And there we have a fully responsive grid doing exactly what we want it to.    

There's is way more to Grid than what I've spoken about above. There are properties to further reduce the amount of code you write (`grid-column: 1/5; grid-row: 5/6`), properties for when you're not sure how many columns or rows you'll need (`grid-template-rows: auto;` or `grid-auto-flow: row`). Grid is huge, it's bigger than flex even, so like I did there, I'm going to pass you of onto a browser game:  

- [CSS-Grid Garden](https://cssgridgarden.com/) is an awesome little browser game where you need to water your plants and and poison the weeds using CSS-Grid rules. 

I also just want to take a moment and mention that CSS-Grid by no means replaces Flexbox. Flex is great for smaller, one-dimensional layouts. For things like navbars and cards. Grid is great for the overall structure, and you should definitely be combining the two to get the absolute best out of both.

- - - -
- - - -

## CSS Frameworks: 
Styling an entire website or app from the ground up can be quite a difficult task, specially if you're the only person building. Luckily, we have CSS Frameworks to help us out.   
So what exactly is a CSS Framework? In simplicity, it's a bunch of pre-styled CSS classes that you can use throughout your app. Of course, we all know it's a little more complicated than this.    
Most of the bigger CSS Frameworks also give you functionality (Modals, dropdowns, pop-overs), help you control responsiveness (how well your elements handle different screen sizes), and cross-browser support (the best part, imo).    
There are *plenty* of CSS Frameworks out there, so how do you choose one? Oftentimes, when starting a new project, this will be one of the first questions we ask. This is a difficult question to answer though, because it really depends of the project itself, as well what the developer has experience with.   
If we're working in Angular (which we mostly are), then there are a few more considerations to take into account:   
Angular doesn't like it when we manipulate the DOM directly. It keeps its own copy of the DOM that it uses to run its famous change detection (a lesson for another time). If we ignore this and manipulate the DOM directly, then Angular's change detection is going to start failing and producing some unwanted results.    

To overcome this, we have two options:  

- We can find a version of our chosen framework that has been wrapped for Angular (this is typically in the form of a series of styled Angular components that you download and call).    
- Or we can use a CSS-Only Framework, that doesn't contain Jquery (or any javascript at all). This does mean, however, that we will have to implement the functionality ourselves.

So let's have a look at some of our options: 

- - - - 
### [Bootstrap](https://getbootstrap.com/docs/4.1/getting-started/introduction/) and [NG-Bootstrap](https://ng-bootstrap.github.io/#/home) 
Bootstrap is the Framework we use in Mabble. And NG-Bootstrap is how we implement the functional classes. In this case, we still import Bootstrap itself into the app for the styled classes, but instead of using Jquery and Popper.js for the functionality, we import the styled components available from NG-Bootstrap.    

Bootstrap is a super useful framework, and really stands out when it comes to its [utility classes](https://getbootstrap.com/docs/4.1/utilities/borders/). 

### [Material Design](https://material.io/) and [Angular Material](https://material.angular.io/) 
Google's Material Design has been around probably the longest. It's not really a framework though, it's actually a design specification built upon UI and UX best practices. It's as much a psychological and scientific document as it is a technical document. However, plenty of Frameworks have been created from it, and the one that we are particularly interested in is Angular Material.    

Angular Material is a very opinionated framework, making life difficult for those that try to design outside of it's specifications (for example, it takes a hack workaround to properly change the background color to anything other than white or grey). Having said that however, Angular Material is definitely one of the more visually appealing frameworks on the list, and it offers a series of animations on almost all of its components that's just about unrivalled. Definitely one to take a look at (In fact, if you pay attention next time you're looking something up on the [Angular Docs](https://angular.io/docs), you might just notice that they themselves have styled their app with Angular Material.)  

### [PrimeNG](https://www.primefaces.org/primeng/) 
Primefaces' PrimeNG is a set of styled components built specifically for Angular, rather than adapted from a more generic framework.   
For this reason, it integrates better with Angular than many of the others, and comes with some seriously cool UI components baked in (Things like Drag and Drop, Charts, File uploader, and a bunch more).   

### [Bulma](https://bulma.io/documentation/) 
Bulma is one of those *CSS-Only* frameworks I was talking about earlier. It doesn't come with *any* Javascript, which makes it absolutely safe to use with Angular.   
Bulma offers quite a lot in the way of UI components, and a lot of display mechanisms (`.box`, `.container`, `.card`, `.tile`, etc).  
It also offers an extremely easy-to-read syntax, making it easier for other developers to figure out what's going on. 

([An example of a basic Navbar built with Bulma](https://codepen.io/Mike8327/pen/MBPzYz).)

Using the functionality of the bulma classes is generally fairly easy, for example to display a modal on click:  

HTML:
```
<div class="card has-text-centered">
  <div class="card-content">
    <button (click)="showModal = true" class="button is-small is-outlined is-info">
      Open Modal
    </button>
  </div>
</div>

<div class="modal" [class.is-active] = "showModal">
  <div class="modal-background"></div>
  <div class="modal-content">
    <div class="box has-text-centered">
      Hello, I am a box... In a modal!  
    </div>
  </div>
  <button (click)="showModal = false" class="modal-close is-large"></button>
</div>
```

TS:
```
showModal = false;
```
([CodePen](https://codepen.io/Mike8327/pen/gjBZey) (Note: Angular doesn't run in CodePen, so the example uses javascript instead).)
   