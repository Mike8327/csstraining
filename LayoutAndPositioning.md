## Layout and Positioning
### FlexBox
Alright, let's talk about layout and positioning. 
One of the most commonly asked questions in css/html forums is:    
`I need to center this div/box/sentence horizontally and vertically, but it isn't working. Anyone know how to do it?`     
and the answer every time?    
`Use Flexbox`    

So what is Flexbox? We won't go into too much detail here, but it is part of the CSS spec and has [almost full cross-browser support](https://caniuse.com/#search=flex) (Quick note, that website, [caniuse.com/](https://caniuse.com/), is an absolute godsend in determining cross-browser support). Of course, IE is the one that only has partial support, but the bugs are minor and there are plenty of workarounds for the few edge-cases.    

Flex-box allows us to define a flex-container that will position *it's direct children* according to various rules you can set. There is a hell of a lot to know about flex-box, so I'm going to cover the basics and leave the more advanced stuff for... others...   

So let's start with the flex-container. 
Consider the following:    

```
<section class="site-header">
	<img src="path/to/image" class="site-logo"/>
	<ul class="navbar">
		<li class="link link-one link-active">
			Link One
		</li>
		<li class="link link-two">
			Link Two
		</li>
		<li class="link link-three">
			Link Three
		</li>
		<li class="link link-four">
			Link Four
		</li>
	</ul>
	<button class="github-button">
		<a href="https://github.com/user">Github</a> 
	</button>
</section>
```

Here we have a basic header containing a logo, a navbar, and a button that links to the project on github. Pretty standard stuff. We can use flex to easily lay those items out in a logical way. Before we start, take a look at the DOM tree there, notice how we have three levels: the parent container, the three separate elements of that container, and then specific children of those elements. This is important because a flex container applies it's rules only to direct children of itself. Let's see the CSS:    

```
.site-header {
	display: flex;
	justify-content: space-between;
	align-items: center;
}
```
So far, so good. Those three simple rules will take our three children and align them horizontally (`display: flex;(\*)`) and vertically (`align-items: center`) and then position them evenly on the horizontal axis using the space *between* (`justify-content: space-between`) them. However, because flex-containers only apply to direct children, that navbar of ours is still going to be displaying vertically. Good thing we can nest flex-containers!
 
(\* Actually, `display: flex;` isn't what's causing the horizontal alignment. `display: flex;` just creates the flex container, and one of the rules of a flex container is `flex-direction`, which is `row` by default. You'll see an example of this later on.)

```
.site-header {
	display: flex;
	justify-content: space-between;
    align-items: center;
}

.navbar {
	display: flex;
	justify-content: space-around;
}
```
([CodePen](https://codepen.io/Mike8327/pen/RBYqYy))

And there we go. We've defined our `<ul>` as a flex-container, and added `justify-content: space-around` to make sure that the space is distributed evenly *around* each item. 
This is a rather simple example, but you can appreciate the idea.    

Flex can be used to create almost any one-dimensional layout you can imagine. Let's look at another example quick:   

```
<section class="site-header-hero">
	<div class="hero-top">
		<img src="path/to/image" class="logo">
		<nav class="navbar">
			<a class="link link-one">
				Link One
			</a>
			<a class="link link-two">
				Link Two
			</a>
			<a class="link link-three">
				Link Three
			</a>
		</nav>
	</div>
	<div class="hero-middle">
		<h1 class="site-title">Welcome to ProjectCSS</h1>
		<button class="call-to-action"><a href="https://github.com/user">Check us out on Github!</a></button>
	</div>
	<div class="hero-bottom">
		<div class="related-links">
			<a class="r-link link-1">Link 1</a>
			<a class="r-link link-2">Link 2</a>
			<a class="r-link link-3">Link 3</a>
		</div>
	</div>
</section>
```

Ok, so that's a bit more than before. Here, we're going for a large Hero section, with a logo and navbar along the top, the site title and call-to-action in the middle, and some related links along the bottom. Let's see how flexbox can help us:   

```
.site-header-hero {
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
}

.hero-top {
  display: flex;
  justify-content: space-between;
}

.navbar {
  display: flex;
  justify-content: space-between;
}

.hero-middle {
  display: flex;
  flex-direction: column;
}

.related-links {
  display: flex;
  justify-content: space-around;
}
```
([CodePen](https://codepen.io/Mike8327/pen/VBGRgQ).)

The first thing to note is the `flex-direction` property on `.site-header-hero` and on `.hero-middle`. By default, that property is set to `row`, here we set it to `column` so that it lays out its children vertically. Also note that when using `flex-direction: column`, your axes become switched, so `justify-content` will align your items vertically, and `align-items` will align your content horizontally.    

There is *a lot* more to talk about when it comes to flex. Instead of typing it all out however, I'm going to pass it off to some other people who have done a way better job with this lesson than I ever could:  

- [Flexbox Froggy](https://flexboxfroggy.com/) is a little browser game to teach you the basics of flex. Help the frog get to his lily-pad using flex commands.  
- When you've finished helping old froggy get home, go test what you've learned with [Flexbox Zombies](https://mastery.games/p/flexbox-zombies). You'll have to create an account and login to get to the game, but it's 12 chapters, each with a few theory lessons and 7 or 8 exercises to teach you everything there is to know about flexbox. (I've only gotten 60% of the way through that one, it's not a short game).   

- - - - 

### CSS-Grid
Flexbox is great for one-dimensional layouts, and sometimes for two-dimensional layouts. But also not really. That sounds confusing but here's the thing: 
In Flexbox, **_The content dictates the layout_**. This isn't very noticeable in small projects or on fast internet connections, but when you use flexbox to layout an entire page, you will find shifting content as the page loads on slower connections. This is because flex-items grow and shrink to fit their containers.   

In CSS-Grid, **_The Container dictates the layout_** (to a degree). You see, when you define a CSS-Grid, you actively define the size and layout of the page. 

Before we continue, we should just note quickly that according to [CanIUse](https://caniuse.com/#search=css%20grid%20layout), grid is almost at full support (IE, you bastard). 

Let me give you an example. Consider the following HTML:

```
<section class="page">
	<header class="header">
		Header
	</header>
	<aside class="sidebar">
		Sidebar
	</aside>
	<div class="content">
		Content
	</div>
	<footer class="footer">
		Footer
	</footer>
</section>
```
What we have there is a basic page layout defining a header, a sidebar, a content area, and a footer. Creating that layout with flex would be possible, but it would involve a lot of work, and when you started filling it with content it would become difficult to keep track of which flex rules are impacting which children. let's see how CSS-Grid handles this:   

```
.page {
	display: grid;
	grid-template-columns: 360px 360px 360px 360px;
	grid-template-rows: 200px 200px 200px 200px 200px;
	grid-gap: 2px;
}
```
Alright cool, we've defined a grid that'll have 4 columns of 360px each, and 6 rows of 200px each. Before we carry on, let's just discuss how horrifically hard-coded this is. I've done it like this on purpose to show off exactly how explicit and strict you can be with your grid, even though most of the time you won't want to. Also, note how much text there is. If I wanted to create a grid the 50 columns, would I have to write out each column?     
Well no, there are actually a ton of abstractions around this. Your first options is the CSS `repeat()` function. This functions takes 2 arguments: the number of times you want to repeat, and the item that you want to repeat. So knowing this, our example up top might look something like this:   

```
.page {
	display: grid;
	grid-template-columns: repeat(4, 25%);
	grid-template-rows: repeat(6, 200px);
	grid-gap: .3rem;
}
```

Alright, straight off the bat we can see the `repeat()` function in action, separating our columns into 4 of 25% width, and rows into 6 of 200px height. This is better, but still not ideal.     
This might be the best time to have a quick sidebar about size units in CSS. We all know what a `px` is (that's one pixel on the screen). We know that percent refers to the percentage of the screen, and we've all used `rem` even if we're not 100% sure why it's better than `px`. You might also have heard of `em`, `vh`, `vw`, `vmin`, `fr`, `pt`, `pc`, `cm`, `mm`, `ex`, `in` and holy hell let's just stop already. As you can see, there are plenty of ways to size your elements with CSS. So which way is the best way? Well, that's a tough one to answer.    
- `rem` units are measured against the font-size of your root element (in most cases that's your `<html>` tag).
- `em` units are measured against the font-size of the direct parent element. 
- `vh` and `vw` measures 1/100th of the viewport height and width respectively (very useful for responsive design if used carefully). `vmin` measures against whichever of the other two are shortest. 
- `fr` measure a fraction of the space available. 


For now we'll leave it at these as they are the most common and useful. You can have a look [here](https://www.tutorialspoint.com/css/css_measurement_units.htm) to see what all the others mean (and maybe even find a good use-case for one I haven't thought of yet).  

So, back to CSS-Grid. How can we make sure our grid is responsive and easy to type out? Let's have a look: 

```
.page {
	width: 100%; // To ensure that the columns take up the full width of the page
	display: grid;
	grid-template-columns: repeat(4, 1fr);
	grid-template-rows: repeat(6, 1fr);
	grid-gap: .3rem;
}
```
Alright, looking better. That `1fr` is telling the browser that each of the 4 columns must take up 1 fraction of the space available. This means that our grid-gap of .3rem will be able to fill up space without pushing the edge of our grid off the page.    
Finally, on to the rest of the grid: 

```
.header {
	grid-column-start: 1;
	grid-column-end: 5;
	grid-row-start: 0;
	grid-row-end: 1;
}

.sidebar {
	grid-column-start: 0;
	grid-column-end: 2;
	grid-row-start: 1;
	grid-row-end: 5;
}

.content {
	grid-column-start: 2;
	grid-column-end: 5;
	grid-row-start: 1;
	grid-row-end: 5;
}

.footer {
	grid-column-start: 1;
	grid-column-end: 5;
	grid-row-start: 5;
	grid-row-end: 6;
}
```
([CodePen](https://codepen.io/Mike8327/pen/MBqMRa))

And there we have a fully responsive grid doing exactly what we want it to.    

There's is way more to Grid than what I've spoken about above. There are properties to further reduce the amount of code you write (`grid-column: 1/5; grid-row: 5/6`), properties for when you're not sure how many columns or rows you'll need (`grid-template-rows: auto;` or `grid-auto-flow: row`). Grid is huge, it's bigger than flex even, so like I did there, I'm going to pass you of onto a browser game:  

- [CSS-Grid Garden](https://cssgridgarden.com/) is an awesome little browser game where you need to water your plants and and poison the weeds using CSS-Grid rules. 

I also just want to take a moment and mention that CSS-Grid by no means replaces Flexbox. Flex is great for smaller, one-dimensional layouts. For things like navbars and cards. Grid is great for the overall structure, and you should definitely be combining the two to get the absolute best out of both. 

- - - -
- - - -